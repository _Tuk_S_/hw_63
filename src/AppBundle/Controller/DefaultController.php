<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class DefaultController extends Controller
{
    /**
     * @Route("/{_locale}", name="homepage",
     *     defaults={"_locale": "en_US"},
     *     requirements = {"_locale" : "en|ru|fr|en_US|fr_FR|ru_RU"})
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function indexAction(Request $request)
    {
        $name = $this->getUser();
        $you_can_add = $this->get('translator')->trans('"%name%" you can add a new phrase', ['%name%' => $name]);
        $title = $this->get('translator')->trans('List of phrases in Russian');
        $register = $this->get('translator')->trans('Register');
        $login = $this->get('translator')->trans('Login');
        $add_phrase = $this->get('translator')->trans('Add phrase');
        $save = $this->get('translator')->trans('Save');

        $form = $this->createFormBuilder()
            ->add('ruName', TextType::class, ['label' => $add_phrase])
            ->add('save', SubmitType::class, ['label' => $save])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $category = new Category();
            $category->translate('ru')->setPhrase($data['ruName']);
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);

            $category->mergeNewTranslations();
            $em->flush();

            return $this->redirectToRoute('homepage');
        }

        $categories = $this->getDoctrine()
            ->getRepository('AppBundle:Category')->findAll();

        return $this->render('AppBundle:default:index.html.twig', [
            'form' => $form->createView(),
            'categories' => $categories,
            'user' => $this->getUser(),
            'loc' => $request->getLocale(),
            'title' => new Response($title),
            'login' => new Response($login),
            'register' => new Response($register),
            'you_can_add' => new Response($you_can_add),
        ]);
    }

    /**
     * @Route("/translate/{_locale}/{id}",
     *     requirements = {"_locale" : "en|ru|fr|en_US|fr_FR|ru_RU", "id": "\d+"}),
     * @Method({"GET", "POST"})
     * @param $id integer
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */

    public function translateAction(Request $request, int $id)
    {
        $name = $this->getUser();
        $title = $this->get('translator')
            ->trans('Translation of phrases in languages English, French, Italian and Spanish');

        $phrase_ru = $this->get('translator')->trans('In Russian');
        $phrase_en = $this->get('translator')->trans('In English');
        $phrase_fr = $this->get('translator')->trans('In French');
        $phrase_it = $this->get('translator')->trans('In Italian');
        $phrase_is = $this->get('translator')->trans('In Spanish');
        $save = $this->get('translator')->trans('Save');
        $back = $this->get('translator')->trans('Back');
        $add_translate = $this->get('translator')
            ->trans('"%name%" you can add phrase translations', ['%name%' => $name]);


        $translations = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:Category')
            ->find($id);

        $form = $this->createFormBuilder()
            ->add('enName', TextType::class, [
                'label' => $phrase_en,
                'attr' => [
                    'value' => $translations->translate('en')->getPhrase() ?: '(No translation)',
//                    'placeholder' => 'english'
                ]
            ])
            ->add('frName', TextType::class, [
                'label' => $phrase_fr,
                'attr' => ['value' => $translations->translate('fr')
                    ->getPhrase() ?: '(Pas de traduction)']
            ])
            ->add('itName', TextType::class, [
                'label' => $phrase_it,
                'attr' => ['value' => $translations->translate('it')
                    ->getPhrase() ?: '(Nessuna traduzione)']
            ])
            ->add('isName', TextType::class, [
                'label' => $phrase_is,
                'attr' => ['value' => $translations->translate('is')
                    ->getPhrase() ?: '(Sin traducción)']
            ])
            ->add('save', SubmitType::class, ['label' => $save])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $translations->translate('en')->setPhrase($data['enName']);
            $translations->translate('fr')->setPhrase($data['frName']);
            $translations->translate('is')->setPhrase($data['isName']);
            $translations->translate('it')->setPhrase($data['itName']);
            $em = $this->getDoctrine()->getManager();
            $em->persist($translations);
            $translations->mergeNewTranslations();
            $em->flush();
            return $this->redirectToRoute('app_default_translate', ['id' => $id]);
        }

        return $this->render('AppBundle:default:translation.html.twig', [
            'form' => $form->createView(),
            'translations' => $translations,
            'user' => $name,
            'title' => new Response($title),
            'phrase_ru' => new Response($phrase_ru),
            'phrase_en' => new Response($phrase_en),
            'phrase_fr' => new Response($phrase_fr),
            'phrase_it' => new Response($phrase_it),
            'phrase_is' => new Response($phrase_is),
            'add_translate' => new Response($add_translate),
            'back' => new Response($back),
        ]);
    }
}

<?php

namespace AppBundle\Features\Context;

use Behat\Behat\Tester\Exception\PendingException;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext implements KernelAwareContext
{
    /** @var  KernelInterface */
    private $kernel;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }




    /**
     * @When /^я нахожусь на главной странице$/
     */
    public function яНахожусьНаГлавнойСтранице()
    {
        $this->visit($this->getContainer()->get('router')->generate('homepage'));
    }

    /**
     * @When /^я вижу слово "([^"]*)" где\-то на странице$/
     */
    public function яВижуСловоГдеТоНаСтранице($login)
    {
        $this->assertPageContainsText($login);
    }


    /**
     * @When /^я кликаю по ссылке "([^"]*)" там$/
     */
    public function яКликаюПоСсылкеТам($login)
    {
        $this->clickLink($login);
    }

    /**
     * @When /^ввожу имя "([^"]*)" и пароль "([^"]*)" там$/
     */
    public function ввожуИмяИПарольТам($username, $password)
    {
//        $user = $this->getContainer()
//            ->get('fos_user.user_manager')
//            ->findUserByUsername($username);
        $this->fillField('_username', $username);
        $this->fillField('_password', $password);

        $this->pressButton('_submit');
    }



    /**
     * @When /^я добавляю фразы "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)" на русском языке$/
     */
    public function яДобавляюФразыНаРусскомЯзыке($arg1, $arg2, $arg3, $arg4, $arg5, $arg6, $arg7, $arg8, $arg9, $arg10)
    {
        $array = [
            $arg1, $arg2, $arg3, $arg4, $arg5, $arg6, $arg7, $arg8, $arg9, $arg10
        ];
        $i = 0;
        foreach ($array as $value) {
            $this->fillField('form[ruName]', $array[$i]);
            $this->pressButton('form[save]');
            $i++;
        }
    }

    /**
     * @When /^я добавляю перевод фразы на английском "([^"]*)" на французском "([^"]*)" на итальянском "([^"]*)" на испанском "([^"]*)" языках$/
     */
    public function яДобавляюПереводФразыНаАнглийскомНаФранцузскомНаИтальянскомНаИспанскомЯзыках($latter_en, $latter_fr, $latter_it, $latter_is)
    {
        $this->fillField('form[enName]', $latter_en);
        $this->fillField('form[frName]', $latter_fr);
        $this->fillField('form[itName]', $latter_it);
        $this->fillField('form[isName]', $latter_is);
        $this->pressButton('form[save]');
    }



    /**
     * @When /^я нажимаю на кнопку "([^"]*)"$/
     */
    public function яНажимаюНаКнопку($button)
    {
        $this->clickLink($button);
    }

    protected function getContainer()
    {
        return $this->kernel->getContainer();
    }


    /**
     * @When /^я уснул на (\d+) секунд$/
     */
    public function яУснулНаСекунд($seconds)
    {
        sleep($seconds);
    }

    /**
     * @When /^я меняю локаль "([^"]*)" на выбранный язык$/
     */
    public function яМеняюЛокальНаВыбранныйЯзык($locale)
    {
        $this->clickLink($locale);
    }


    /**
     * Sets Kernel instance.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }


}
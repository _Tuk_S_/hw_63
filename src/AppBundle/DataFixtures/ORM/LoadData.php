<?php
/**
 * Created by PhpStorm.
 * User: tuk
 * Date: 05.08.17
 * Time: 14:26
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Category;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;


    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user
            ->setUsername('admin')
            ->setRoles(["ROLE_ADMIN"])
            ->setEmail('original@some.com')
            ->setEnabled(true);
        $encoder = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($user, '123');
        $user->setPassword($password);
        $manager->persist($user);

        $category1 = new Category;
        $category1->translate('fr')->setPhrase('Beaucoup dhommes ont peur des femmes, à qui ils ont besoin pour se développer, il est plus facile dutiliser ceux auxquels il est possible de descendre ...');
        $category1->translate('en')->setPhrase('Many men are afraid of women, to whom they need to grow, it is easier to use those to which you can go down ...');
        $category1->translate('ru')->setPhrase('Многие мужчины боятся женщин, до которых им нужно дорасти, проще пользоваться теми, до которых можно опуститься…');
        $category1->translate('it')->setPhrase('Molti uomini hanno paura delle donne, a cui hanno bisogno per crescere, è più facile da usare quelli a cui è possibile scendere ...');
        $category1->translate('is')->setPhrase('Muchos hombres tienen miedo de las mujeres, a los que necesitan para crecer, es más fácil de usar aquellos en los que es posible descender ...');
        $manager->persist($category1);
        $category1->mergeNewTranslations();
        $category1->translate('en')->getPhrase();

        $category2 = new Category;
        $category2->translate('ru')->setPhrase('Мы обижаемся, ссоримся и злимся лишь на тех, кого так боимся потерять, кого искренне любим.');
        $category2->translate('fr')->setPhrase('Nous sommes offensés, querelle en colère et seuls ceux qui ont tellement peur de perdre qui aiment sincèrement.');
        $category2->translate('is')->setPhrase('Nos ofende, pelea enojado y sólo aquellos que son mucho miedo de perder que sinceramente aman.');
        $category2->translate('it')->setPhrase('Siamo offesi, lite arrabbiato e solo coloro che sono così paura di perdere che amano sinceramente.');
        $category2->translate('en')->setPhrase('We take offense, we quarrel and are angry only at those whom we are so afraid to lose, who we sincerely love.');
        $manager->persist($category2);
        $category2->mergeNewTranslations();// рекомендуется использовать указанный метод перед методом flush
        $category2->translate('en')->getPhrase();

        $category3 = new Category;
        $category3->translate('ru')->setPhrase('Всё меняется. Меняется жизнь. Меняются люди. И вроде всё хорошо. Но иногда так не хватает старого. Той жизни. Тех людей.');
        $category3->translate('is')->setPhrase('To do cambia. Cambiando vidas. La gente cambia. Y como todos los buenos. Pero a veces no es suficiente de la edad. Que la vida. Aquellas personas.');
        $category3->translate('en')->setPhrase('Everything changes. Life is changing. People are changing. And like all is well. But sometimes it is not enough of the old. That life. Of those people.');
        $category3->translate('it')->setPhrase('Tutto cambia. Cambiare vita. Le persone cambiano. E come tutti i buoni. Ma a volte non è sufficiente del vecchio. Che la vita. Quelle persone.');
        $category3->translate('fr')->setPhrase('Tout change. Changer la vie. Les gens changent. Et comme tout bon. Mais parfois, il ne suffit pas de lancien. Que la vie. Ces personnes.');
        $manager->persist($category3);
        $category3->mergeNewTranslations();
        $category3->translate('en')->getPhrase();

        $manager->flush();
    }
}

//php bin/console doctrine:fixtures:load